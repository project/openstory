DESCRIPTION
-----------
* OpenStory is an open source editorial interface for Drupal sites, that 
offers a more user-friendly way for editors to create and edit all forms 
of digital content.
  
  Created with Angular and NodeJS, OpenStory aims to improve the editorial 
  experience on Drupal 8 through an interface created especially for 
  content management, comments management, and user management 
  functionalities.
  
  Decoupled interface specially created to simplify content creation!
  
  More info about openSTORY can be found here: https://openstory.io

DEPENDENCIES
------------
* OpenStory is a module based on REST services and JSON API implemented with 
OAuth 2.0, such that it has dependencies on modules RESTful Web Services,
 JSON API, REST UI, HTTP Basic Authentication, HAL and Simple OAuth.

  Other dependencies are Search API for searching words in the site, Activity
  for tracking what people do and Contact storage for working with contact
  forms.

INSTALLATION
-----------
* Install the module using Composer: composer require drupal/openstory. 
You can use any other installation method, as long as you install other
dependencies needed, like the OAuth2 Server composer package.

CONFIGURATION
-------------
* After enabling the module, please configure the module Simple OAuth
as it says in the README of the module or by following the next steps:  
   * Install the module using Composer. If you installed the module OpenStory
   via composer, then there's nothing left to do. If not, please make sure you
   install the [OAuth2 Server](https://github.com/thephpleague/oauth2-server)
   composer package.
   * Generate a pair of keys to encrypt the tokens. And store them outside
   of your document root for security reasons.
   ```
   openssl genrsa -out private.key 2048
   openssl rsa -in private.key -pubout > public.key
   ```
   * Save the path to your keys in: `/admin/config/people/simple_oauth`.
   * Create a Client Application by going to:
    `/admin/config/services/consumer/add`.
   
* These are the required user role permissions that you need in order
  to successfully manage the CMS content through the Openstory interface:
  * Comment
    * All permissions
  * Content Moderation if installed
    * All permissions
  * File
    *  Access the Files overview page
  * File entity
    * Add and upload new files
    * View files
    * View own files
  * Node
    * Add permissions for all contents wanted (Create, Edit, Delete)
    * Administer content
    * Administer content types
    * View published content
    * View own published content
  
  * RESTful Web Services
    * Access DELETE on Remove Oauth token resource
    * Access GET on Count stories resource
    * Access GET on Get all roles resource
    * Access GET on Get entity list resource
    * Access GET on OpenStory Clear Cache resource
    * Access GET on OpenStory Run Cron resource
    * Access GET on Validate Oauth Token resource
    * Access POST on Reset user password resource
    
  * Simple Oauth/Simple Oauth Extras
    * All permissions, except Administer Access Token entities
  * Taxonomy
    * Add permissions for all taxonomies wanted (Create, Edit, Delete)
  * User
    * Administer users
    * Administer permissions
    * View user information
  * Open Story
    * Token Validation - Add this permission to Authenticated user.

REST ENDPOINTS
-------------
* The module includes preconfigured REST endpoints which enable
the Angular Interface to communicate with the Drupal Backend.
All resources are authorised through an OAuth 2.0 consumer token.
The resources included in the module are:
  * Drupal\openstory\Plugin\rest\resource\ClearCacheResource;
    This resource can only receive GET type requests with no parameters
    on the path "canonical" = "/os_clear_cache"
    This resource clears all Drupal caches.
  * Drupal\openstory\Plugin\rest\resource\CountStories;
    This resource only receives GET type requests on the path
    "canonical" = "/get_count_stories/{days}" where {days} is an integer.
    The functionality is tested with days:1 and it returnsa count of all nodes
    ass a whole but also grouped by bundle and the number of stories created
    "today". Also for content types, it returns the delta which is the report
    between what was created "today" and what was created "yesterday" grouped by
    content type.
  * Drupal\openstory\Plugin\rest\resource\CreateEntities;
    This resource extends Drupal core's entity resource and is accessible on the path
    "canonical" = "/create_entities/{entity_type}/{entity}".
    It only accepts POST and PATCH as methods and it is used to extend the functionality already
    provided by core by enriching the saved information.
  * Drupal\openstory\Plugin\rest\resource\GetEntitiesListResource;
    This resource only accepts GET requests and can be accessed on the path
    "canonical" = "/get_entity_list".
    It returns information about all of the present Entities of different types
    (for example: existing nodes or taxonomy terms).
  * Drupal\openstory\Plugin\rest\resource\GetRolesResource; 
    This resource is accessible on path "canonical" = "/get_roles".
    It only accepts GET requests and it returns all the role names
    present whithin the site.
  * Drupal\openstory\Plugin\rest\resource\RemoveTokenLogoutResource;
    This resource is accessible on path "canonical" = "/remove_oauth_token"
    and it accepts DELETE requests.
    The role of this resource is to delete all expired oauth tokens.
  * Drupal\openstory\Plugin\rest\resource\ResetUserPassword;
    This resource can be accessed on path "canonical" = "/reset_user_password".
    It only accepts POST requests and is used to reset the Drupal user password
    through the Angular Interface.
  * Drupal\openstory\Plugin\rest\resource\RunCronResource;
    This functionality of this resource can be invoked by sending a
    GET request on path "canonical" = "/os_run_cron".
    This resource will run all cron jobs from the Drupal site.

ATTENTION
-------------
* If you encounter the following error: TypeError: Argument 1 passed to
Drupal\jsonapi\Normalizer\EntityReferenceFieldNormalizer::isInternalResourceType()
must implement interface Drupal\Core\Entity\EntityInterface, null given, called
in /docroot/modules/contrib/jsonapi/src/Normalizer/
EntityReferenceFieldNormalizer.php on line 138 in 
Drupal\jsonapi\Normalizer\EntityReferenceFieldNormalizer->isInternalResourceType()
(line 253 of /docroot/modules/contrib/jsonapi/src/Normalizer/
EntityReferenceFieldNormalizer.php) please apply the patch from 
https://www.drupal.org/project/token/issues/2978262. This is an error 
related to token module and appears when trying to POST an entity.
The entity is created, even though you will get 500 server error.

* Also, if you are using link fields on your entities, you have to apply 
the following patch: https://www.drupal.org/project/drupal/issues/2982060.

* In order to get the right permissions for users, it is mandatory
to have no role from the simple oauth consumer and to apply 
the following patch: 
https://www.drupal.org/files/issues/provide_default_scopes-2857930-2.patch
