<?php

namespace Drupal\openstory;

use Drupal\Core\Entity\EntityTypeManager;

/**
 * Provides the entity.
 */
class EntityStructure {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Check if user has permission.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The entity bundle.
   *
   * @return string
   *   Permission name.
   */
  public function getUserPermissionName($entity_type, $bundle) {
    switch ($entity_type) {
      case 'node':
        $permissionName = 'create ' . $bundle . ' content';
        break;

      case 'taxonomy_term':
        $permissionName = 'edit terms in ' . $bundle;
        break;

      case 'user':
        $permissionName = 'administer users';
        break;

      case 'contact_message':
        $permissionName = 'administer contact forms';
        break;

      case 'comment':
        $permissionName = 'post comments';
        break;

      default:

    }
    return $permissionName;
  }

}
