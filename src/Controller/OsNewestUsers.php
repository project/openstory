<?php

namespace Drupal\openstory\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\jsonapi\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class OsNewestUsers.
 *
 * @package Drupal\openstory\Controller
 */
class OsNewestUsers extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * OsNewestUsers constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Return how many users are new.
   *
   * @param string $days
   *   Filter parameter.
   *
   * @return \Drupal\jsonapi\ResourceResponse
   *   Return how many users are new.
   */
  public function newUsers($days) {
    $countNewestUsers = 0;
    $date = strtotime("-" . $days . " days");
    if ($date !== FALSE) {
      $query = $this->entityTypeManager->getStorage('user')->getQuery();
      $query->condition('created', $date, '>');
      $countNewestUsers = $query->count()->execute();
    }
    return new JsonResponse($countNewestUsers);

  }

}
