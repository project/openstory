<?php

namespace Drupal\openstory\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\jsonapi\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class OsGoogleAnalytics.
 *
 * @package Drupal\openstory\Controller
 */
class OsGoogleAnalytics extends ControllerBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * OsActiveUsers constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler) {
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * Check if google analytics is set.
   *
   * @return \Drupal\jsonapi\ResourceResponse
   *   Return boolean.
   */
  public function isSetGoogleAnalytics() {
    $isSet = FALSE;
    if ($this->moduleHandler->moduleExists('google_analytics')) {
      $config = \Drupal::config('google_analytics.settings');
      if (!empty($config) && !empty($config->getRawData())) {
        $data = $config->getRawData();
        if (!empty($data['account'])) {
          $isSet = TRUE;
        }
      }
    }
    return new JsonResponse($isSet);
  }

}
