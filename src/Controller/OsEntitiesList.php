<?php

namespace Drupal\openstory\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\jsonapi\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class OsEntitiesList.
 *
 * @package Drupal\openstory\Controller
 */
class OsEntitiesList extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user query param.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * OsEntitiesList constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Symfony\Component\HttpFoundation\Request $currentRequest
   *   The current request.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, Request $currentRequest) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentRequest = $currentRequest;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Returns information about all entity types on the systems.
   *
   * @param string $entityType
   *   The entity type.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   *
   * @return \Drupal\jsonapi\ResourceResponse
   *   Return the definition via jsonapi.
   */
  public function entitiesList($entityType) {
    $type_infos = [];
    $entityDefinitions = $this->entityTypeManager->getDefinitions();
    switch ($entityType) {
      case 'node':
        $this->getListOfSpecificEntity($entityDefinitions[$entityType], $entityType, $type_infos);
        break;

      case 'taxonomy_term':
        $this->getListOfSpecificEntity($entityDefinitions[$entityType], $entityType, $type_infos);
        break;

      case 'contact_message':
        $this->getListOfSpecificEntity($entityDefinitions[$entityType], $entityType, $type_infos);
        break;

      default:
        $this->getListOfSpecificEntity($entityDefinitions['node'], 'node', $type_infos);
        $this->getListOfSpecificEntity($entityDefinitions['taxonomy_term'], 'taxonomy_term', $type_infos);
        $this->getListOfSpecificEntity($entityDefinitions['contact_message'], 'contact_message', $type_infos);
    }

    $response = new JsonResponse($type_infos, 200);
    return $response;

  }

  /**
   * Gets the list of specified entity.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $entity_type_id
   *   The entity type id.
   * @param array $type_infos
   *   The array returned.
   */
  protected function getListOfSpecificEntity($entity_type, $entity_type_id, array &$type_infos) {
    if ($bundle_entity_type_id = $entity_type->getBundleEntityType()) {
      $bundles = $this->entityTypeManager->getStorage($bundle_entity_type_id)->loadMultiple();
      if ($entity_type_id == 'node') {
        $allContents = [];
        foreach ($bundles as $key => $value) {
          if (!empty($value->get('name'))) {
            $allContents[$key] = $value->get('name');
          }
        }
        $type_infos[$entity_type_id]['bundles'] = $allContents;
      }
      else {
        $type_infos[$entity_type_id]['bundles'] = array_keys($bundles);
      }
    }
  }

}
