<?php

namespace Drupal\openstory\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\jsonapi\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Connection;

/**
 * Class OsFilesListing.
 *
 * @package Drupal\openstory\Controller
 */
class OsFilesListing extends ControllerBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The current user query param.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The image factory service.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * The connection to the database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * OsFilesListing constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request.
   * @param \Drupal\Core\Image\ImageFactory $imageFactory
   *   The image factory service.
   * @param \Drupal\Core\Database\Connection $database
   *   The connection to the database.
   */
  public function __construct(FileSystemInterface $file_system, Request $current_request, ImageFactory $imageFactory, Connection $database) {
    $this->fileSystem = $file_system;
    $this->currentRequest = $current_request;
    $this->imageFactory = $imageFactory;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('image.factory'),
      $container->get('database')
    );
  }

  /**
   * Return files with details from mediaLibrary folder.
   *
   * @return \Drupal\jsonapi\ResourceResponse
   *   Return details about files.
   */
  public function filesListing() {
    $files = [];

    // Get query parameters.
    $filemime = $this->currentRequest->query->get('filemime');
    $start_date = $this->currentRequest->query->get('start_date');
    $end_date = $this->currentRequest->query->get('end_date');
    $start_limit = $this->currentRequest->query->get('start_limit');
    $end_limit = $this->currentRequest->query->get('end_limit');

    // Create query to get files.
    $query = $this->database->select('file_managed', 'fileManaged');
    $query->leftJoin('file_usage', 'fileUsage', 'fileUsage.fid = fileManaged.fid');
    if (!empty($filemime)) {
      $orCondition = $query->orConditionGroup();
      foreach ($filemime as $tempFilemime) {
        $orCondition->condition('fileManaged.filemime', $tempFilemime . '%', 'LIKE');
      }
      $query->condition($orCondition);
    }
    if (!empty($start_date) && !empty($end_date)) {
      $orConditionDate = $query->orConditionGroup();
      foreach ($start_date as $key => $value) {
        if (!empty($end_date[$key])) {
          $orConditionDate->condition('fileManaged.created', [$start_date[$key], $end_date[$key]], 'BETWEEN');
        }
      }
      $query->condition($orConditionDate);
    }
    if ((!empty($start_limit) || $start_limit === '0') && !empty($end_limit)) {
      $query->range($start_limit, $end_limit);
    }
    $query->fields('fileManaged', ['fid']);
    $query->groupBy('fileManaged.fid', 'fileManaged.filename', 'fileManaged.type', 'fileManaged.filemime', 'fileManaged.filesize', 'fileManaged.status', 'fileManaged.changed', 'fileManaged.created');
    $result = $query->execute()->fetchCol();

    $count = 0;
    foreach ($result as $fid) {
      $file = File::load($fid);
      $files[$count]['filename'] = $file->getFilename();
      $files[$count]['uri'] = file_create_url($file->getFileUri());
      $files[$count]['filesize'] = $file->getSize();
      $files[$count]['filemime'] = $file->getMimeType();
      $mimeType = $file->getMimeType();
      // Get image width and height.
      if (strpos($mimeType, 'image') !== FALSE) {
        $image = $this->imageFactory->get($file->getFileUri());
        $files[$count]['width'] = $image->getWidth();
        $files[$count]['height'] = $image->getHeight();
      }
      $files[$count]['created'] = $file->getCreatedTime();
      if (!empty($file->getOwner())) {
        $files[$count]['username'] = $file->getOwner()->getAccountName();
      }
      $count++;
    }
    return new JsonResponse($files, 200);
  }

}
