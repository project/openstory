<?php

namespace Drupal\openstory\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\Entity\BaseFieldOverride;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class OsEntityDefinition.
 *
 * @package Drupal\openstory\Controller
 */
class OsEntityDefinition extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Base fields needed.
   *
   * @var array
   */
  protected $baseFields = [
    'promote' => 'promote',
    'status' => 'status',
    'title' => 'title',
    'sticky' => 'sticky',
    'name' => 'name',
    'mail' => 'mail',
    'pass' => 'pass',
    'roles' => 'roles',
    'path' => 'path',
  ];

  /**
   * OsEntityDefinition constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $fieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $fieldManager, ModuleHandlerInterface $moduleHandler) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fieldManager = $fieldManager;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * Returns information about all entity types on the systems.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   *
   * @return \Drupal\jsonapi\ResourceResponse
   *   Return the definition via jsonapi.
   */
  public function entityDefinition($entityType, $bundle) {

    if (!empty($entityType) && !empty($bundle)) {
      $definition = $this->getDefinition($entityType, $bundle);

      $entity = $this->entityTypeManager->getDefinition($entityType);
      if ($bundle_entity_type_id = $entity->getBundleEntityType()) {
        $entityBundle = $this->entityTypeManager->getStorage($bundle_entity_type_id)->load($bundle);
        $definition['entityLabel'] = $entityBundle->label();
      }

      // Add base fields to entity definition.
      $definitions = $this->fieldManager->getFieldDefinitions($entityType, $bundle);
      foreach ($definitions as $fieldName => $fieldDefinition) {
        if ($fieldDefinition instanceof BaseFieldDefinition || $fieldDefinition instanceof BaseFieldOverride) {
          if (!array_key_exists($fieldName, $this->baseFields)) {
            continue;
          }
          $fieldInfo = [
            'isBaseField' => TRUE,
            'label' => $fieldDefinition->getLabel(),
            'type' => $fieldDefinition->getType(),
            'description' => $fieldDefinition->getDescription(),
            'required' => $fieldDefinition->isRequired(),
            'readonly' => $fieldDefinition->isReadOnly(),
            'cardinality' => $fieldDefinition->getFieldStorageDefinition()
              ->getCardinality(),
            'settings' => $fieldDefinition->getSettings(),
          ];
          $definitions = $fieldDefinition->toArray();
          if (!empty($definitions) && !empty($definitions['default_value'])) {
            $fieldInfo['default_value'] = $definitions['default_value'];
          }
          else {
            // Add for roles, the default values.
            if ($entityType === 'user' && $fieldName === 'roles') {
              $roles = array_map(['\Drupal\Component\Utility\Html', 'escape'], user_role_names(TRUE));
              $fieldInfo['default_value'] = $roles;
            }
          }

          if ($fieldName === 'status') {
            if ($this->moduleHandler->moduleExists('content_moderation')) {
              $fieldInfo['content_moderation'] = TRUE;
            }
            else {
              $fieldInfo['content_moderation'] = FALSE;
            }
          }

          if ($fieldName === 'path') {
            if ($this->moduleHandler->moduleExists('path')) {
              $fieldInfo['path'] = TRUE;
            }
            else {
              $fieldInfo['path'] = FALSE;
            }
          }
          
          $definition[$fieldName] = $fieldInfo;
        }
      }

      $response = new JsonResponse($definition, 200);
      return $response;
    }
    else {
      throw new BadRequestHttpException('Parameters entity type and bundle are mandatory.');
    }

  }

  /**
   * Get entity definition.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   *
   * @return array
   *   The definition.
   */
  public function getDefinition($entityType, $bundle) {
    if (!$this->entityTypeManager->hasDefinition($entityType)) {
      throw new NotFoundHttpException($this->t('No entity type found: @type.', ['@type' => $entityType]));
    }
    $definition = [];
    // Create the entity to normalize it via json api.
    $entity = $this->getEntity($entityType, $bundle);
    $alreadyCreated = FALSE;
    if (!is_array($entity)) {
      $entity->save();
    }
    else {
      // Key 1 is the entity loaded.
      $entity = $entity[1];
      $alreadyCreated = TRUE;
    }

    if (!empty($entity)) {
      // Check if bundle exists in case the entity type is a node.
      $contentEntityType = $this->entityTypeManager->getDefinition($entityType);
      if ($bundleEntityTypeId = $contentEntityType->getBundleEntityType()) {
        $entityBundle = $this->entityTypeManager->getStorage($bundleEntityTypeId)->load($bundle);
        if (!$entityBundle) {
          throw new NotFoundHttpException($this->t('No bundle "@bundle" found for entity type @type',
            ['@type' => $entityType, '@bundle' => $bundle]
          )
          );
        }
      }

      // Get the view display of the content type via json api.
      $entityViewDisplay = \Drupal::service('jsonapi_extras.entity.to_jsonapi')->normalize($entity);

      $fields = $this->getEntityFields($entityViewDisplay);

      if (!empty($fields)) {
        foreach ($fields as $key => $value) {
          // Get fieldStorage and normalize it.
          $fieldStorage = $this->entityTypeManager->getStorage('field_storage_config')->load($entityType . '.' . $value);

          if (!empty($fieldStorage)) {
            if ($fieldStorage->get('type') === 'comment') {
              continue;
            }
          }

          if (!empty($fieldStorage)) {
            $fieldStorageNormalized = \Drupal::service('jsonapi_extras.entity.to_jsonapi')->normalize($fieldStorage);

            $this->getStorageProperties($fieldStorageNormalized, $definition);
          }

          // Get field config and normalize it.
          $fieldConfig = $this->entityTypeManager->getStorage('field_config')->load($entityType . '.' . $bundle . '.' . $value);
          if (!empty($fieldConfig)) {
            $fieldConfigNormalized = \Drupal::service('jsonapi_extras.entity.to_jsonapi')->normalize($fieldConfig);
            $this->getConfigProperties($fieldConfigNormalized, $definition);
          }
          // If the field name exists as property in definition,
          // check if it is a complex field.
          if (!empty($definition) && array_key_exists($value, $definition)) {
            if (!empty($definition[$value]['storage'])) {
              // See if there are nested fields.
              if (!empty($definition[$value]['storage']['type'])) {
                switch ($definition[$value]['storage']['type']) {
                  case 'field_collection':
                    $this->getNestedFields($definition, $value, 'field_collection');
                    break;

                  case 'entity_reference_revisions':
                    $this->getNestedFields($definition, $value, 'entity_reference_revisions');
                    break;

                  case 'datetime':
                    if (!empty($definition[$value]['config'])) {
                      $config = $definition[$value]['config'];
                      $definition[$value]['config']['osTimeZone'] = date_default_timezone_get();
                      if (!empty($config['default_value'])) {
                        $newDefaultValue = [];
                        foreach ($config['default_value'] as $def => $defValue) {
                          $temp = [];
                          $temp['default_date_type'] = $defValue['default_date_type'];
                          $temp['default_date'] = strtotime($defValue['default_date']);
                          array_push($newDefaultValue, $temp);
                        }
                        $definition[$value]['config']['default_value'] = $newDefaultValue;
                      }
                    }
                    break;

                  case 'timestamp':
                    $definition[$value]['config']['osTimeZone'] = date_default_timezone_get();
                    break;

                  case 'metatag':
                    $this->getMetaTagDefinition($definition, $value, $entity, $entityType, $bundle);
                    break;

                  default:
                }
              }
            }
          }
        }
      }
    }
    if (!$alreadyCreated) {
      $entity->delete();
    }
    return $definition;
  }

  /**
   * Get nested fields.
   *
   * @param array $definition
   *   Entity definition.
   * @param string $fieldName
   *   Get the fields of this field.
   * @param string $nestedEntityType
   *   Entity type.
   */
  public function getNestedFields(array &$definition, $fieldName, $nestedEntityType) {
    switch ($nestedEntityType) {
      case 'field_collection':
        $nestedDefinition = $this->getDefinition('field_collection_item', $fieldName);
        $definition[$fieldName]['nested_fields'][] = $nestedDefinition;
        break;

      case 'entity_reference_revisions':
        $this->getEntityNestedFields($definition, $fieldName, 'paragraph');
        break;

      default:
    }
  }

  /**
   * Get the nested fields of a complex field.
   *
   * @param array $definition
   *   Entity definition.
   * @param string $fieldName
   *   Get nested fields of this field.
   * @param string $entity
   *   Entity type.
   */
  public function getEntityNestedFields(array &$definition, $fieldName, $entity) {
    if (!empty($definition[$fieldName]['config']) && !empty($definition[$fieldName]['config']['settings'])) {
      $contents = $definition[$fieldName]['config']['settings']['handler_settings']['target_bundles'];
      if (!empty($contents)) {
        foreach ($contents as $content => $contentValue) {
          $nestedDefinition = $this->getDefinition($entity, $content);
          if (!empty($nestedDefinition)) {
            $definition[$fieldName]['nested_fields'][] = $nestedDefinition;
          }
        }
      }
    }
  }

  /**
   * Get the entity.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Return the entity.
   */
  public function getEntity($entityType, $bundle) {
    switch ($entityType) {
      case 'node':

        $searchForExistingOnes = $this->entityTypeManager->getStorage('node')->loadByProperties([
          'type' => $bundle,
        ]);
        if (!empty($searchForExistingOnes)) {
          $turner = [TRUE, array_values($searchForExistingOnes)[0]];
          return $turner;
        }

        return $this->entityTypeManager->getStorage('node')->create(['type' => $bundle, 'title' => $bundle]);

      case 'taxonomy_term':
        return $this->entityTypeManager->getStorage('taxonomy_term')->create(['vid' => $bundle, 'name' => $bundle . 'Mame']);

      case 'user':
        return $this->entityTypeManager->getStorage('user')->create([
          'name' => 'serialization_test_user',
          'mail' => 'foo@example.com',
          'pass' => '123456',
        ]);

      case 'comment':
        $values = [];
        $definition = $this->entityTypeManager->getDefinition("comment");
        $bundle_key = $definition->get('entity_keys')['bundle'];
        $values[$bundle_key] = $bundle;

        $comment = $this->entityTypeManager->getStorage('comment')->loadByProperties([$bundle_key => $bundle]);
        if (!empty($comment)) {
          $turner = [TRUE, array_values($comment)[0]];
          return $turner;
        }
        else {
          $compatibleEntity = [];
          // Get all content type names.
          $types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
          $possibleFields = [];
          if (!empty($types)) {
            $types = array_keys($types);
            foreach ($types as $indexOf => $type) {
              $nodeFields = $this->fieldManager->getFieldDefinitions('node', $type);
              if (!empty($nodeFields)) {
                foreach ($nodeFields as $nodeIndexOf => $fieldTmp) {
                  $fieldConfiguration = $fieldTmp->getConfig($type);
                  if (!empty($fieldConfiguration)) {
                    $fieldType = $fieldConfiguration->get('field_type');
                    if ($fieldType === 'comment') {
                      $tmpStorageConfig = $fieldConfiguration->get('dependencies');
                      $tmpStorageConfig['node.field_name'] = 'node.' . $fieldConfiguration->get('field_name');
                      $tmpStorageConfig['field_name'] = $fieldConfiguration->get('field_name');
                      $tmpStorageConfig['bundle'] = $type;
                      $possibleFields[] = $tmpStorageConfig;
                    }
                  }
                }
              }
            }
            if (!empty($possibleFields)) {
              foreach ($possibleFields as $possibleFieldIndex => $storageData) {
                if (isset($storageData['config'])) {
                  foreach ($storageData['config'] as $configIndexOf => $dependencies) {
                    if (strpos($dependencies, 'storage') !== FALSE) {
                      $fieldStorageConfiguration = $this->entityTypeManager
                        ->getStorage('field_storage_config')
                        ->load($storageData['node.field_name']);
                      if (!empty($fieldStorageConfiguration)) {
                        $commentTypeSettings = $fieldStorageConfiguration->get('settings');
                        if (!empty($commentTypeSettings)) {
                          if (key($commentTypeSettings) === $bundle_key) {
                            if ($commentTypeSettings[$bundle_key] === $bundle) {
                              $compatibleEntity = [
                                'entity_type' => 'node',
                                'field_name'  => $storageData['field_name'],
                                'uid' => 1,
                                'subject' => $bundle,
                                'comment_body' => $bundle,
                                'comment_type' => $bundle,
                                  // TODO: Remove(unset) this further down below.
                                'node_bundle' => $storageData['bundle'],
                              ];
                              break;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
              if (!empty($compatibleEntity)) {
                $finalNodeBundle = $compatibleEntity['node_bundle'];
                unset($compatibleEntity['node_bundle']);
                $nodeAttacher = $this->entityTypeManager->getStorage('node')->loadByProperties(['type' => $finalNodeBundle]);
                if (!empty($nodeAttacher)) {
                  $id = array_keys($nodeAttacher)[0];
                  $compatibleEntity['entity_id'] = $id;
                  return $this->entityTypeManager->getStorage('comment')->create($compatibleEntity);
                }
              }
            }
          }
        }

      case 'contact_message':
        return $this->entityTypeManager->getStorage('contact_message')->create([
          'contact_form' => $bundle,
        ]);

      case 'field_collection_item':
        return $this->entityTypeManager->getStorage('field_collection_item')->create([
          'field_name' => $bundle,
        ]);

      case 'paragraph':
        return $this->entityTypeManager->getStorage('paragraph')->create([
          'type' => $bundle,
        ]);

      default:
    }

  }

  /**
   * Get entity fields.
   *
   * @param array $entityViewDisplay
   *   The entity view display.
   *
   * @return array
   *   Return the entity fields.
   */
  public function getEntityFields(array $entityViewDisplay) {
    $fields = [];
    $attributes = $this->checkJsonApiFormat($entityViewDisplay, 'attributes');
    if (!empty($attributes)) {
      foreach ($attributes as $item => $value) {
        // No need base fields.
        if (array_key_exists($item, $this->baseFields)) {
          continue;
        }
        array_push($fields, $item);
      }
    }
    $relationships = $this->checkJsonApiFormat($entityViewDisplay, 'relationships');
    if (!empty($relationships)) {
      foreach ($relationships as $item => $value) {
        // No need base fields.
        if (array_key_exists($item, $this->baseFields)) {
          continue;
        }
        array_push($fields, $item);
      }
    }
    return $fields;
  }

  /**
   * Add to definition properties needed.
   *
   * @param array $fieldStorageNormalized
   *   Normalized field storage via jsonapi.
   * @param array $definition
   *   Variable that hold the entity definition.
   */
  public function getStorageProperties(array $fieldStorageNormalized, array &$definition) {
    $attributes = $this->checkJsonApiFormat($fieldStorageNormalized, 'attributes');
    $attributes['id'] = $attributes['drupal_internal__id'];
    $attributes['type'] = $attributes['field_storage_config_type'];
    $attributes['uuid'] = $fieldStorageNormalized['data']['id'];
    $definition[$attributes['field_name']]['storage'] = $attributes;

  }

  /**
   * Add to definition properties needed.
   *
   * @param array $fieldConfigNormalized
   *   Normalized field config via jsonapi.
   * @param array $definition
   *   Variable that hold the entity definition.
   */
  public function getConfigProperties(array $fieldConfigNormalized, array &$definition) {
    $attributes = $this->checkJsonApiFormat($fieldConfigNormalized, 'attributes');
    $definition[$attributes['field_name']]['config'] = $attributes;
  }

  /**
   * Check if it is the format expected.
   *
   * @param array $json
   *   The json result from jsonapi.
   * @param string $property
   *   The property needed to check if exists in $json.
   *
   * @return mixed
   *   Return property array.
   */
  public function checkJsonApiFormat(array $json, $property) {
    if (is_array($json)) {
      if (array_key_exists('data', $json)) {
        if (is_array($json['data'])) {
          if (array_key_exists($property, $json['data'])) {
            return $json['data'][$property];
          }
        }
      }
    }
  }

  /**
   * Build Metatag field definition groups.
   *
   * @param array $definition
   *   Entity definition.
   * @param string $fieldName
   *   Entity field name.
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   Processed entity.
   * @param string $entityType
   *   Entity type.
   * @param string $bundle
   *   Entity bundle.
   *
   * @return void
   */
  public function getMetaTagDefinition(&$definition, $fieldName, $entity, $entityType, $bundle) {
    if ($this->moduleHandler->moduleExists('metatag')) {
      $metatag_manager = \Drupal::service('metatag.manager');
      $metatag_settings = $this->config('metatag.settings');
      $field_values = $metatag_manager->tagsFromEntityWithDefaults($entity);
      $groups_and_tags = $metatag_manager->sortedGroupsWithTags();
      $entity_type_groups = $metatag_settings->get('entity_type_groups');
      $tag_plugin_manager = \Drupal::service('plugin.manager.metatag.tag');
      $groups = !empty($entity_type_groups[$entityType]) &&
        !empty($entity_type_groups[$entityType][$bundle]) ?
        $entity_type_groups[$entityType][$bundle] : [];
      $included_groups = $groups;
      // Build the definitions with the metatag form processing.
      $meta_groups = [];
      $index = 0;
      // Use the Metatag form builder code to get the field definitions in separate groups.
      foreach ($groups_and_tags as $group_name => $group) {
        // Only act on groups that have tags and are in the list of included
        // groups (unless that list is null).
        if (isset($group['tags']) && (is_null($included_groups) ||
          in_array($group_name, $included_groups) || in_array($group['id'], $included_groups))) {
          // Create the fieldset.
          $meta_groups[$group_name]['type'] = 'details';
          $meta_groups[$group_name]['index'] = $index;
          $meta_groups[$group_name]['title'] = $group['label'];
          $meta_groups[$group_name]['description'] = $group['description'];
          $meta_groups[$group_name]['fields'] = [];
          foreach ($group['tags'] as $tag_name => $tag) {
            // Only act on tags in the included tags list, unless that is null.
            if (is_null($included_tags) || in_array($tag_name, $included_tags) ||
            in_array($tag['id'], $included_tags)) {
              // Make an instance of the tag.
              $tag = $tag_plugin_manager->createInstance($tag_name);
              $tag_form = $tag->form($meta_groups);
              // Set the value to the stored value, if any.
              $tag_value = isset($field_values[$tag_name]) ? $field_values[$tag_name] : NULL;
              $tag->setValue($tag_value);
              $tag_formatted = [];
              $tag_formatted['id'] = $tag->id();
              $tag_formatted['name'] = $tag->name();
              $tag_formatted['title'] = $tag->label();
              $tag_formatted['description']= $tag->description();
              $tag_formatted['group'] = $tag->group();
              $tag_formatted['weight'] = $tag->weight();
              $tag_formatted['type'] = $tag->type();
              $tag_formatted['secure'] = $tag->secure();
              $tag_formatted['multiple'] = $tag->multiple();
              $tag_form = $tag->form($meta_groups);
              if($tag_form) {
                $tag_formatted['required'] = $tag_form['#required'] ? $tag_form['#required']: false;
                $tag_formatted['type'] = $tag_form['#type']? $tag_form['#type'] : $tag_formatted['type'];
                $tag_formatted['default_value'] = $tag_form['#default_value']?
                  $tag_form['#default_value']: $tag->value();
                $tag_formatted['type'] = $tag_form['#type']? $tag_form['#type']: $tag_formatted['type'];
                $tag_formatted['maxlength'] = $tag_form['#maxlength']? $tag_form['#maxlength']:255;
              }
              // Create the bit of form for this tag.
              $meta_groups[$group_name]['fields'][$tag_name] = $tag_formatted;
            }
          }
          $index++;
        }
      }
      if($definition[$fieldName]['config']) {
        $definition[$fieldName]['config']['default_value'] = $field_values;
        $definition[$fieldName]['config']['meta_groups'] = $meta_groups;
      }
    }
  }

}
