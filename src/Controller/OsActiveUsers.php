<?php

namespace Drupal\openstory\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\jsonapi\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class OsActiveUsers.
 *
 * @package Drupal\openstory\Controller
 */
class OsActiveUsers extends ControllerBase {

  /**
   * The connection to the database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * OsActiveUsers constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The connection to the database.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Return how many users are active.
   *
   * @return \Drupal\jsonapi\ResourceResponse
   *   Return how many users are active.
   */
  public function activeUsers() {
    $query = $this->database->select('sessions');
    $query->groupBy('uid');
    $countActiveUsers = $query->countQuery()->execute()->fetchField();
    return new JsonResponse($countActiveUsers);
  }

}
