<?php

namespace Drupal\openstory\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\jsonapi\ResourceResponse;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class OsTokenValidate.
 *
 * @package Drupal\openstory\Controller
 */
class OsTokenValidate extends ControllerBase {

  /**
   * Check if token is valid or not.
   *
   * @return \Drupal\jsonapi\ResourceResponse
   *   Return references.
   */
  public function isTokenValid() {

    return new JsonResponse('Is valid', 200);

  }

}
