<?php

namespace Drupal\openstory\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\jsonapi\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Connection;

/**
 * Class OsSearchReferences.
 *
 * @package Drupal\openstory\Controller
 */
class OsSearchReferences extends ControllerBase {

  /**
   * The connection to the database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * OsSearchReferences constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The connection to the database.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Return references based on entity type and bundle.
   *
   * @return \Drupal\jsonapi\ResourceResponse
   *   Return references.
   */
  public function searchReferences() {
    $references = [];
    $content = \Drupal::request()->getContent();
    if (!empty($content)) {
      $body = json_decode($content);
      $entityType = $body->entityType;
      $bundles = $body->bundles;
      if (!empty($entityType) && !empty($bundles)) {
        switch ($entityType) {
          case 'node':
            $query = $this->database->select('node_field_data', 'nfd');
            $query->innerJoin('node', 'n', 'nfd.nid = n.nid');
            $query->condition('nfd.type', $bundles, 'IN');
            $query->addField('nfd', 'title', 'label');
            $query->addField('n', 'uuid', 'id');
            $query->addField('n', 'type', 'targetType');
            $result = $query->execute()->fetchAll();
            break;

          case 'taxonomy_term':
            $query = $this->database->select('taxonomy_term_data', 'ttd');
            $query->innerJoin('taxonomy_term_field_data', 'ttfd', 'ttd.tid = ttfd.tid');
            $query->condition('ttd.vid', $bundles, 'IN');
            $query->addField('ttfd', 'name', 'label');
            $query->addField('ttd', 'uuid', 'id');
            $query->addField('ttd', 'vid', 'targetType');
            $result = $query->execute()->fetchAll();
            break;

          case 'user':
            $query = $this->database->select('users_field_data', 'ufd');
            $query->innerJoin('users', 'u', 'ufd.uid = u.uid');
            $query->addField('ufd', 'name', 'label');
            $query->addField('u', 'uuid', 'id');
            $result = $query->execute()->fetchAll();
            if (!empty($result) && !empty($result[0])) {
              if (empty($result[0]->label)) {
                $result[0]->label = 'Anonymous';
              }
            }
            break;

          default:
        }
      }
    }
    if (!empty($result)) {
      foreach ($result as $key) {
        switch ($entityType) {
          case 'node':
            $references[] = [
              'id' => $key->id,
              'label' => $key->label,
              'targetType' => $entityType,
              'targetBundle' => $key->targetType,
            ];
            break;

          case 'taxonomy_term':
            $references[] = [
              'id' => $key->id,
              'label' => $key->label,
              'targetType' => $entityType,
              'targetBundle' => $key->targetType,
            ];
            break;

          case 'user':
            $references[] = [
              'id' => $key->id,
              'label' => $key->label,
              'targetType' => $entityType,
              'targetBundle' => 'user',
            ];
            break;

          default:
        }
      }
    }
    return new JsonResponse($references, 200);
  }

}
