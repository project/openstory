<?php

namespace Drupal\openstory\Entities;

use Drupal\file\Entity\File;
use Drupal\simple_oauth\Entities\AccessTokenEntity;
use Drupal\user\Entity\User;
use Lcobucci\JWT\Builder;
use League\OAuth2\Server\CryptKey;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;

/**
 * Class OpenStoryAccessTokenEntity.
 *
 * @package Drupal\openstory\Entities
 */
class OpenStoryAccessTokenEntity extends AccessTokenEntity {


  private $openStoryClaims = [
    'name' => 'name',
    'mail' => 'mail',
    'user_picture' => 'userPicture',
    'uuid' => 'uuid',
  ];

  /**
   * {@inheritdoc}
   */
  public function convertToJWT(CryptKey $privateKey) {
    $private_claims = [];
    \Drupal::moduleHandler()
      ->alter('simple_oauth_private_claims', $private_claims, $this);
    if (!is_array($private_claims)) {
      $message = 'An implementation of hook_simple_oauth_private_claims_alter ';
      $message .= 'returns an invalid $private_claims value. $private_claims ';
      $message .= 'must be an array.';
      throw new \InvalidArgumentException($message);
    }

    $builder = (new Builder())
      ->permittedFor($this->getClient()->getIdentifier())
      ->identifiedBy($this->getIdentifier(), TRUE)
      ->issuedAt(time())
      ->canOnlyBeUsedAfter(time())
      ->expiresAt($this->getExpiryDateTime()->getTimestamp())
      ->relatedTo($this->getUserIdentifier())
      ->withClaim('scopes', $this->getScopes());


    $uid = $this->getUserIdentifier();
    if ($uid) {
      $user = User::load($uid);
      if ($user) {
        foreach ($this->openStoryClaims as $keyClaim => $valueClaim) {
          if ($user->hasField($keyClaim)) {
            $fieldValue = $user->get($keyClaim)->getValue();
            if (!empty($fieldValue) && !empty($fieldValue[0])) {
              if (!empty($fieldValue[0]['value'])) {
                $builder->withClaim($valueClaim, $fieldValue[0]['value']);
              }
              else {
                if (!empty($fieldValue[0]['target_id'])) {
                  if ($keyClaim == 'user_picture') {
                    $fileId = $fieldValue[0]['target_id'];
                    $file = File::load($fileId);
                    $uri = $file->uri;
                    if ($uri) {
                      $uriValue = $uri->getValue();
                      if ($uriValue[0] && $uriValue[0]['value']) {
                        $builder->withClaim($valueClaim, file_create_url($uriValue[0]['value']));
                      }
                    }
                  }
                  else {
                    $builder->withClaim($valueClaim, $fieldValue[0]['target_id']);
                  }
                }
                else {
                  $builder->withClaim($valueClaim, NULL);
                }
              }
            }
            else {
              $builder->withClaim($valueClaim, NULL);
            }
          }
        }
      }
    }

    foreach ($private_claims as $claim_name => $value) {
      $builder->withClaim($claim_name, $value);
    }

    $key = new Key($privateKey->getKeyPath(), $privateKey->getPassPhrase());
    $token = $builder->getToken(new Sha256(), $key)->getToken();
    return $token;
  }

}
