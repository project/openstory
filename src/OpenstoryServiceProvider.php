<?php

namespace Drupal\openstory;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

/**
 * Class OpenstoryServiceProvider.
 *
 * @package Drupal\openstory
 */
class OpenstoryServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {

  /**
   * Alter service.
   *
   * @param \Drupal\Core\DependencyInjection\ContainerBuilder $container
   *   The container.
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('simple_oauth.repositories.access_token');
    $definition->setClass('Drupal\openstory\Repositories\OpenStoryAccessTokenRepository');
  }

}
