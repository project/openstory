<?php

namespace Drupal\openstory;

use Drupal\views\ViewExecutable;

/**
 * Provides a pager for view result.
 */
class OpenStoryViewPager {

  /**
   * Create pager for view result.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   * @param array $rows
   *   View rows result.
   *
   * @return array
   *   View result.
   */
  public function getViewResult(ViewExecutable $view, array $rows) {
    $pager = $view->pager;
    $result = [];
    if ($pager) {
      $class = get_class($pager);
      $items_per_page = $pager->getItemsPerPage();
      $total_items = $pager->getTotalItems();
      $total_pages = 0;
      if (!in_array($class, ['Drupal\views\Plugin\views\pager\None', 'Drupal\views\Plugin\views\pager\Some'])) {
        $total_pages = $pager->getPagerTotal();
      }
      $result = [
        'rows' => $rows,
        'pager' => [
          'total_items' => $total_items,
          'total_pages' => $total_pages,
          'items_per_page' => $items_per_page,
          'offset' => $pager->getOffset(),
        ],
      ];
    }
    else {
      $result = [
        'rows' => $rows,
        'pager' => [
          'total_items' => '',
          'total_pages' => '',
          'items_per_page' => '',
          'offset' => '',
        ],
      ];
    }

    return $result;
  }

}
