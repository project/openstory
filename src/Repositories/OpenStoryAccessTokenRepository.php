<?php

namespace Drupal\openstory\Repositories;

use Drupal\openstory\Entities\OpenStoryAccessTokenEntity;
use Drupal\simple_oauth\Repositories\AccessTokenRepository;
use League\OAuth2\Server\Entities\ClientEntityInterface;

/**
 * Class OpenStoryAccessTokenRepository.
 *
 * @package Drupal\openstory\Repositories
 */
class OpenStoryAccessTokenRepository extends AccessTokenRepository {

  protected static $entity_class = 'Drupal\openstory\Entities\OpenStoryAccessTokenEntity';

  /**
   * {@inheritdoc}
   */
  public function getNewToken(ClientEntityInterface $client_entity, array $scopes, $user_identifier = NULL) {
    $access_token = new OpenStoryAccessTokenEntity();
    $access_token->setClient($client_entity);
    foreach ($scopes as $scope) {
      $access_token->addScope($scope);
    }
    $access_token->setUserIdentifier($user_identifier);

    return $access_token;
  }

}
