<?php

namespace Drupal\openstory\Plugin\views\style;

use Drupal\openstory\OpenStoryViewPager;
use Drupal\rest\Plugin\views\style\Serializer;
use Drupal\views\Plugin\views\display\Page;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Serializer that return the pagination for a listing page.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "openstory_list_serializer",
 *   title = @Translation("OpenStory List Serializer"),
 *   help = @Translation("OpenStory list serializer for the rest export"),
 *   display_types = {"data"}
 * )
 */
class OsListSerializer extends Serializer {

  /**
   * The view pager.
   *
   * @var \Drupal\openstory\OpenStoryViewPager
   */
  protected $viewPager;

  /**
   * Constructs a Plugin object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The serializer interface.
   * @param array $serializer_formats
   *   The serializer format.
   * @param array $serializer_format_providers
   *   The serializer format providers.
   * @param \Drupal\openstory\OpenStoryViewPager $viewPager
   *   The serializer format providers.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SerializerInterface $serializer, array $serializer_formats, array $serializer_format_providers, OpenStoryViewPager $viewPager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer, $serializer_formats, $serializer_format_providers);
    $this->viewPager = $viewPager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('serializer'),
      $container->getParameter('serializer.formats'),
      $container->getParameter('serializer.format_providers'),
      $container->get('openstory_view_pager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $rows = [];

    foreach ($this->view->result as $row_index => $row) {
      $this->view->row_index = $row_index;
      $rows[] = $this->view->rowPlugin->render($row);
    }
    unset($this->view->row_index);

    // Get the content type configured in the display or fallback to the
    // default.
    if ((empty($this->view->live_preview))) {
      $displayHandler = $this->displayHandler;
      if ($displayHandler instanceof Page) {
        $content_type = !empty($this->options['formats']) ? reset($this->options['formats']) : 'json';
      }
      else {
        $content_type = $this->displayHandler->getContentType();
      }
    }
    else {
      $content_type = !empty($this->options['formats']) ? reset($this->options['formats']) : 'json';
    }

    $result = $this->viewPager->getViewResult($this->view, $rows);

    return $this->serializer->serialize($result, $content_type, ['views_style_plugin' => $this]);
  }

}
