<?php

namespace Drupal\openstory\Plugin\views\style;

use Drupal\comment\Entity\Comment;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\openstory\OpenStoryViewPager;
use Drupal\rest\Plugin\views\style\Serializer;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\display\Page;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Create new serializer that will return the name of a user.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "openstory_entity_serializer",
 *   title = @Translation("OpenStory Entity Serializer"),
 *   help = @Translation("OpenStory entity serializer for the rest export"),
 *   display_types = {"data"}
 * )
 */
class OsEntitySerializer extends Serializer {

  /**
   * The view pager.
   *
   * @var \Drupal\openstory\OpenStoryViewPager
   */
  protected $viewPager;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a Plugin object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The serializer interface.
   * @param array $serializer_formats
   *   The serializer format.
   * @param array $serializer_format_providers
   *   The serializer format providers.
   * @param \Drupal\openstory\OpenStoryViewPager $viewPager
   *   The serializer format providers.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SerializerInterface $serializer, array $serializer_formats, array $serializer_format_providers, OpenStoryViewPager $viewPager, EntityRepositoryInterface $entityRepository, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer, $serializer_formats, $serializer_format_providers);
    $this->viewPager = $viewPager;
    $this->entityRepository = $entityRepository;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('serializer'),
      $container->getParameter('serializer.formats'),
      $container->getParameter('serializer.format_providers'),
      $container->get('openstory_view_pager'),
      $container->get('entity.repository'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Get name of a user.
   */
  public function render() {
    $rows = [];
    foreach ($this->view->result as $row_index => $row) {
      $this->view->row_index = $row_index;
      $renderedRow = $this->view->rowPlugin->render($row);
      $images = [];
      // Check if there is a field image only for nodes.
      if ($renderedRow instanceof Node) {
        foreach ($renderedRow as $item) {
          $fieldDefinition = $item->getFieldDefinition();
          if ($fieldDefinition->getType() == 'image') {
            $itemValue = $item->getValue();
            $imageSettings = $fieldDefinition->getSettings();
            if (!empty($itemValue)) {
              if (!empty($itemValue[0]) && !empty($itemValue[0]['target_id'])) {
                $fileId = $itemValue[0]['target_id'];
                $file = File::load($fileId);
                $uri = $file->uri;
                if ($uri) {
                  $uriValue = $uri->getValue();
                  if ($uriValue[0] && $uriValue[0]['value']) {
                    // Need only the first picture.
                    if (empty($images['osContentPicture'])) {
                      $images['osContentPicture'] = [
                        'id' => $fileId,
                        'url' => file_create_url($uriValue[0]['value']),
                      ];
                    }
                  }
                }
              }
            }
          }
        }
      }
      $currentRow = $renderedRow->toArray();
      if (is_array($currentRow)) {
        if (array_key_exists('uri', $currentRow)) {
          $currentUri = $currentRow['uri'][0]['value'];
          $currentRow['uri'][0]['value'] = file_create_url($currentUri);
        }
      }
      if (!empty($renderedRow->get('uid'))) {
        $uid = $renderedRow->get('uid')->getValue();
        if (!empty($uid[0]) && !empty($uid[0]['target_id'])) {
          $user = User::load($uid[0]['target_id']);
          if (!empty($user)) {
            if ($user->get('user_picture')) {
              $userPicture = $user->get('user_picture');
              $picturePath = '';
              if ($userPicture) {
                $pictureId = $userPicture->getValue();
                if ($pictureId && $pictureId[0] && $pictureId[0]['target_id']) {
                  $file = File::load($pictureId[0]['target_id']);
                  $uri = $file->uri;
                  if ($uri) {
                    $uriValue = $uri->getValue();
                    if ($uriValue[0] && $uriValue[0]['value']) {
                      $picturePath = file_create_url($uriValue[0]['value']);
                    }
                  }
                }
              }
            }
            $currentRow['username']['name'] = $user->getAccountName();
            if ($picturePath) {
              $currentRow['username']['userPicture'] = $picturePath;
            }
            else {
              $currentRow['username']['userPicture'] = '';
            }
          }
          else {
            $currentRow['username']['name'] = 'Anonymous';
            $currentRow['username']['userPicture'] = '';
          }
        }
        else {
          if (!empty($uid[0])) {
            if ($uid[0]['target_id'] === '0') {
              $currentRow['username']['name'] = 'Anonymous';
              $currentRow['username']['userPicture'] = '';
            }
          }
        }
      }
      if ($renderedRow instanceof Node) {
        if ($renderedRow->hasField('nid')) {
          $nidValue = $renderedRow->get('nid')->getValue();
          if (!empty($nidValue[0]) && !empty($nidValue[0]['value'])) {
            $nid = $nidValue[0]['value'];
            if (!empty($nid)) {
              $cids = $this->entityTypeManager->getStorage('comment')
                ->getQuery()
                ->condition('entity_type', 'node')
                ->condition('entity_id', $nid)
                ->execute();
              $currentRow['countComments'] = count($cids);
            }
          }
        }
        // If there is no picture, put empty id and url.
        if (empty($images['osContentPicture'])) {
          $currentRow['osContentPicture'] = ['id' => '', 'url' => ''];
        }
        else {
          $currentRow['osContentPicture'] = $images['osContentPicture'];
        }
      }

      if ($renderedRow instanceof User) {
        if (!empty($renderedRow->get('user_picture'))) {
          $userPicture = $renderedRow->get('user_picture')->getValue();
          $picturePath = '';
          if ($userPicture && !empty($userPicture[0]) && !empty($userPicture[0]['target_id'])) {
            $file = File::load($userPicture[0]['target_id']);
            $uri = $file->uri;
            if ($uri) {
              $uriValue = $uri->getValue();
              if ($uriValue[0] && $uriValue[0]['value']) {
                $picturePath = file_create_url($uriValue[0]['value']);
                $currentRow['user_picture'][0]['url'] = $picturePath;
              }
            }
          }
        }
        foreach ($renderedRow as $item => $itemValue) {
          $fieldDefinition = $itemValue->getFieldDefinition();
          $type = $fieldDefinition->getType();
          if ($type === 'created' || $type === 'changed' || $type === 'timestamp') {
            $value = $itemValue->getValue();
            if (!empty($value) && !empty($value[0]) && !empty($value[0]['value'])) {
              $currentRow[$item][0]['value'] = date('Y-m-d\TH:i:s', $value[0]['value']);
            }
            else {
              $currentRow[$item][0]['value'] = "1970-01-01T00:00:00";
            }
          }
        }
      }
      if ($renderedRow instanceof Comment) {
        if (!empty($renderedRow->get('entity_type')) && !empty($renderedRow->get('entity_id'))) {
          $entityType = $renderedRow->entity_type->value;
          $entityId = $renderedRow->entity_id->target_id;
          if (!empty($entityType) && !empty($entityId)) {
            $entity = $this->entityTypeManager->getStorage($entityType)->load($entityId);
            if (!empty($entity)) {
              $commentContentType = $entity->getType();
              if (!empty($commentContentType)) {
                $commentContentType = $node_type = $this->entityTypeManager->getStorage('node_type')->load($commentContentType)->get('name');
                $currentRow['commentContentType'] = empty($commentContentType) ? '' : $commentContentType;
              }
            }
          }
        }
      }
      $rows[] = $currentRow;
    }

    unset($this->view->row_index);

    // Get the content type configured in the display or fallback to the
    // default.
    if ((empty($this->view->live_preview))) {
      $displayHandler = $this->displayHandler;
      if ($displayHandler instanceof Page) {
        $content_type = !empty($this->options['formats']) ? reset($this->options['formats']) : 'json';
      }
      else {
        $content_type = $this->displayHandler->getContentType();
      }
    }
    else {
      $content_type = !empty($this->options['formats']) ? reset($this->options['formats']) : 'json';
    }

    $result = $this->viewPager->getViewResult($this->view, $rows);

    return $this->serializer->serialize($result, $content_type, ['views_style_plugin' => $this]);
  }

  /**
   * Get fid and url of an image.
   *
   * @param string $fileUuid
   *   The file uuid.
   *
   * @return array
   *   Array with image fid and url.
   */
  private function getFileProperties($fileUuid) {
    if (!empty($fileUuid)) {
      $entity = $this->entityRepository->loadEntityByUuid('file', $fileUuid);
      $fid = $entity->get('fid')->getValue();
      $uri = $entity->get('uri')->getValue();
      if ($fid[0] && $fid[0]['value']) {
        $fidValue = $fid[0]['value'];
      }
      if ($uri[0] && $uri[0]['value']) {
        $uriValue = $uri[0]['value'];
      }
      return [
        'id' => !empty($fidValue) ? $fidValue : '',
        'url' => !empty($uriValue) ? file_create_url($uriValue) : '',
      ];
    }
  }

}
