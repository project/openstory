<?php

namespace Drupal\openstory\Plugin\rest;

use Drupal\comment\Plugin\Field\FieldType\CommentItemInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\Plugin\Type\ResourcePluginManager;
use Drupal\taxonomy\Entity\Term;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The base class for resources returning entity type information.
 */
abstract class EntityTypeResourceBase extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The rest resource manager.
   *
   * @var \Drupal\rest\Plugin\Type\ResourcePluginManager
   */
  protected $resourceManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * Fields that can store other fields (needed nested fields).
   *
   * @var array
   */
  protected $complexFields = [
    'field_collection' => 'field_collection',
    'entity_reference_revisions' => 'entity_reference_revisions',
    'entity_reference' => 'entity_reference',
    'comment' => 'comment',
  ];

  /**
   * Base fields needed.
   *
   * @var array
   */
  protected $baseFields = [
    'promote' => 'promote',
    'status' => 'status',
    'title' => 'title',
    'promote' => 'promote',
    'uid' => 'uid',
    'created' => 'created',
    'sticky' => 'sticky',
    'path' => 'path',
    'name' => 'name',
    'mail' => 'mail',
    'pass' => 'pass',
    'roles' => 'roles',
  ];

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\rest\Plugin\Type\ResourcePluginManager $resource_manager
   *   The rest resource manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   The entity field manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user, EntityTypeManagerInterface $entity_type_manager, ResourcePluginManager $resource_manager, EntityFieldManagerInterface $field_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->resourceManager = $resource_manager;
    $this->fieldManager = $field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('custom_rest'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.rest'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * Gets information on all the fields on the bundle.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $bundleName
   *   The bundle name.
   *
   * @return array
   *   The information about the bundle's fields.
   */
  protected function getBundleFields($entityTypeId, $bundleName) {
    // Store in fields all the necessary properties of the fields.
    $fields = [];
    // Load form display to get weight and hidden properties.
    $entityFormDisplay = $this->entityTypeManager->getStorage('entity_form_display')
      ->load($entityTypeId . '.' . $bundleName . '.default');
    if (!$entityFormDisplay) {
      $values = [
        'targetEntityType' => $entityTypeId,
        'bundle' => $bundleName,
        'mode' => 'default',
        'status' => TRUE,
      ];
      $entityFormDisplay = $this->entityTypeManager->getStorage('entity_form_display')
        ->create($values);
    }

    // Fields definitions of the entity.
    $fieldDefinitions = $this->fieldManager->getFieldDefinitions($entityTypeId, $bundleName);
    foreach ($fieldDefinitions as $fieldName => $fieldDefinition) {
      $fieldType = $fieldDefinition->getType();
      if ($entityFormDisplay) {
        if ($fieldDefinition instanceof BaseFieldDefinition) {
          if (!array_key_exists($fieldName, $this->baseFields)) {
            continue;
          }
          if ($entityTypeId === 'user' && $bundleName === 'user') {
            if ($fieldName === 'uid') {
              continue;
            }
          }
        }
        // Get content property from display form.
        $components = $entityFormDisplay->getComponents();
        // If the field is not in content property, it means is hidden,
        // so don't need it.
        if (isset($components[$fieldName]) || array_key_exists($fieldName, $this->baseFields)) {
          $fieldInfo = [
            'label' => $fieldDefinition->getLabel(),
            'type' => $fieldType,
            'description' => $fieldDefinition->getDescription(),
            'required' => $fieldDefinition->isRequired(),
            'readonly' => $fieldDefinition->isReadOnly(),
            'cardinality' => $fieldDefinition->getFieldStorageDefinition()
              ->getCardinality(),
            'settings' => $fieldDefinition->getSettings(),
          ];
          // Set the weight property.
          if (isset($components[$fieldName]['weight'])) {
            $fieldInfo['weight'] = $components[$fieldName]['weight'];
          }
          // Set the type.
          if ($components[$fieldName]['type']) {
            $fieldInfo['base_type'] = $components[$fieldName]['type'];
          }
          else {
            // Is base field type.
            $fieldInfo['base_type'] = $fieldType;
          }
          // Set default value for non base fields.
          if ($fieldDefinition instanceof FieldConfig) {
            $fieldInfo['default_value'] = $fieldDefinition->get('default_value');
            switch ($fieldType) {
              case 'comment':
                $defaultValue = $fieldDefinition->get('default_value');
                $customDefaultValue = $this->commentStatuses($defaultValue);
                $fieldInfo['default_value'] = $customDefaultValue;
                break;

              case 'entity_reference':
                $references = $this->loadEntityReference($fieldDefinition);
                $fieldInfo['references'] = $references;
                break;

              case 'datetime':
                $defaultDate = $fieldDefinition->get('default_value');
                $dateArray = [];
                if (!empty($defaultDate)) {
                  foreach ($defaultDate as $date) {
                    $d = [];
                    if (!empty($date['default_date_type']) && $date['default_date_type'] === 'relative') {
                      $d['default_date_type'] = 'relative';
                      $d['default_date'] = strtotime($date['default_date']);
                      $dateArray[] = $d;
                    }
                    elseif (!empty($date['default_date_type']) && $date['default_date_type'] === 'now') {
                      $dateArray[] = $date;
                    }
                  }
                }
                $fieldInfo['default_value'] = $dateArray;
                break;

              default:

            }
            if ($fieldType == 'text_with_summary' || $fieldType == 'text_long' || $fieldType == 'text') {
              // If the field is textarea add the allowed formats.
              $allFormats = filter_formats();
              $fieldInfo['allowed_formats'] = array_keys($allFormats);
            }
          }
          // Set default value for base field.
          elseif ($fieldDefinition instanceof BaseFieldDefinition) {
            $definitions = $fieldDefinition->toArray();
            if (!empty($definitions) && !empty($definitions['default_value'])) {
              $fieldInfo['default_value'] = $definitions['default_value'];
            }
          }

          // If field type exists in complexFields variable,
          // recall function to get also the fields from that entity.
          if (array_key_exists($fieldType, $this->complexFields)) {
            $fieldsFieldInfo = $this->getFieldsComplexField($fieldType, $fieldName, $fieldDefinition);
            if (!empty($fieldsFieldInfo)) {
              $fieldInfo['nested_fields'] = $fieldsFieldInfo;
            }
          }
          $fields[$fieldName] = $fieldInfo;
        }
      }

    }
    return $fields;
  }

  /**
   * Gets fields information of needed entity.
   *
   * @param string $fieldType
   *   The field type.
   * @param string $fieldName
   *   The field name.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The field definition.
   *
   * @return array
   *   The fields information.
   */
  protected function getFieldsComplexField($fieldType, $fieldName, FieldDefinitionInterface $fieldDefinition) {
    switch ($fieldType) {
      case 'field_collection':
        return $this->getBundleFields('field_collection_item', $fieldName);

      case 'entity_reference_revisions':
        $settings = $fieldDefinition->getSettings();
        if (!empty($settings)) {
          $targetBundle = $settings['handler_settings']['target_bundles'];
          if (!empty($targetBundle)) {
            $fields = [];
            foreach ($targetBundle as $item) {
              $fields[] = $this->getBundleFields('paragraph', $item);
            }
            return $fields;
          }
        }
        break;

      case 'comment':
        $fieldStorageDefinition = $fieldDefinition->getFieldStorageDefinition();
        if (!empty($fieldStorageDefinition)) {
          $settings = $fieldStorageDefinition->getSettings();
          if (!empty($settings)) {
            $commentType = $settings['comment_type'];
            return $this->getBundleFields('comment', $commentType);
          }
        }
        break;

      default:
        return [];
    }
  }

  /**
   * Adds new properties to default value.
   *
   * @param array $defaultValue
   *   The default value of a field.
   *
   * @return array
   *   The default value of a field.
   */
  protected function commentStatuses(array $defaultValue) {
    foreach ($defaultValue as $key => $value) {
      switch ($value['status']) {
        case CommentItemInterface::OPEN:
          $defaultValue[$key]['statusTitle'] = t('Open');
          $defaultValue[$key]['statusDescription'] = t('Users with the "Post comments" permission can post comments.');
          break;

        case CommentItemInterface::CLOSED:
          $defaultValue[$key]['statusTitle'] = t('Closed');
          $defaultValue[$key]['statusDescription'] = t('Users cannot post comments, but existing comments will be displayed.');
          break;

        case CommentItemInterface::HIDDEN:
          $defaultValue[$key]['statusTitle'] = t('HIDDEN');
          $defaultValue[$key]['statusDescription'] = t('Comments are hidden from view.');
          break;

        default:
      }
      return $defaultValue;
    }
  }

  /**
   * Load entity reference.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The field definition.
   *
   * @return array
   *   An array containing all nodes/terms of the bundle specified.
   */
  protected function loadEntityReference(FieldDefinitionInterface $fieldDefinition) {
    $result = [];
    $settings = $fieldDefinition->getSettings();
    if (!empty($settings)) {
      $handler = $settings['handler'];
      $targetBundle = $settings['handler_settings']['target_bundles'];
      if (strpos($handler, 'node') !== FALSE) {
        if (!empty($targetBundle)) {
          foreach ($targetBundle as $item) {
            $nids = \Drupal::entityQuery('node')->condition('type', $item)->execute();
            $nodes = Node::loadMultiple($nids);
            foreach ($nodes as $nid => $node) {
              $result[$nid] = $node->getTitle();
            }
          }
        }
      }
      elseif (strpos($handler, 'views') !== FALSE) {
        $handlerSettings = $settings['handler_settings'];
        if (!empty($handlerSettings['view'])) {
          $viewSettings = $handlerSettings['view'];
          if (!empty($viewSettings['view_name']) && !empty($handlerSettings['view']['display_name'])) {
            $view = views_get_view_result($viewSettings['view_name'], $viewSettings['display_name']);
            if (!empty($view)) {
              foreach ($view as $row => $rowValue) {
                $entity = $view[$row]->_entity;
                if ($entity instanceof Node) {
                  $nid = $view[$row]->nid;
                  $result[$nid] = $entity->getTitle();
                }
                elseif ($entity instanceof Term) {
                  $tid = $view[$row]->tid;
                  $result[$tid] = $entity->getName();
                }
              }
            }
          }
        }
      }
      else {
        if (strpos($handler, 'taxonomy_term') !== FALSE) {
          if (!empty($targetBundle)) {
            foreach ($targetBundle as $item) {
              $tids = \Drupal::entityQuery('taxonomy_term')->condition('vid', $item)->execute();
              $terms = Term::loadMultiple($tids);
              foreach ($terms as $tid => $term) {
                $result[$tid] = $term->getName();
              }
            }
          }
        }
      }
    }
    return $result;
  }

}
