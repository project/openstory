<?php

namespace Drupal\openstory\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a resource to get a list of entity types.
 *
 * @RestResource(
 *   id = "get_entity_list",
 *   label = @Translation("Get entity list"),
 *   uri_paths = {
 *     "canonical" = "/get_entity_list"
 *   }
 * )
 */
class GetEntitiesListResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user query param.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user, EntityTypeManagerInterface $entity_type_manager, Request $current_request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRequest = $current_request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $configuration,
        $plugin_id,
        $plugin_definition,
        $container->getParameter('serializer.formats'),
        $container->get('logger.factory')->get('custom_rest'),
        $container->get('current_user'),
        $container->get('entity_type.manager'),
        $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Returns information about all entity types on the systems.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {
    $type_infos = [];
    $entityDefinitions = $this->entityTypeManager->getDefinitions();
    $param = $this->currentRequest->query->get('entity_type_id');
    switch ($param) {
      case 'node':
        $this->getListOfSpecificEntity($entityDefinitions[$param], $param, $type_infos);
        break;

      case 'taxonomy_term':
        $this->getListOfSpecificEntity($entityDefinitions[$param], $param, $type_infos);
        break;

      case 'contact_message':
        $this->getListOfSpecificEntity($entityDefinitions[$param], $param, $type_infos);
        break;

      default:
        $this->getListOfSpecificEntity($entityDefinitions['node'], 'node', $type_infos);
        $this->getListOfSpecificEntity($entityDefinitions['taxonomy_term'], 'taxonomy_term', $type_infos);
        $this->getListOfSpecificEntity($entityDefinitions['contact_message'], 'contact_message', $type_infos);
    }

    $response = new ResourceResponse($type_infos, 200);
    $response->addCacheableDependency($param);
    return $response;
  }

  /**
   * Gets the list of specified entity.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $entity_type_id
   *   The entity type id.
   * @param array $type_infos
   *   The array returned.
   */
  protected function getListOfSpecificEntity($entity_type, $entity_type_id, array &$type_infos) {
    if ($bundle_entity_type_id = $entity_type->getBundleEntityType()) {
      $bundles = $this->entityTypeManager->getStorage($bundle_entity_type_id)->loadMultiple();
      if ($entity_type_id == 'node') {
        $allContents = [];
        foreach ($bundles as $key => $value) {
          if (!empty($value->get('name'))) {
            $allContents[$key] = $value->get('name');
          }
        }
        $type_infos[$entity_type_id]['bundles'] = $allContents;
      }
      else {
        $type_infos[$entity_type_id]['bundles'] = array_keys($bundles);
      }
    }
  }

}
