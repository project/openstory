<?php

namespace Drupal\openstory\Plugin\rest\resource;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Validate fields, but not those unsupported from server.
 *
 * @internal
 * @todo Consider making public in https://www.drupal.org/node/2300677
 */
trait CreateEntityResourceValidationTrait {

  /**
   * Verifies that the whole entity does not violate any validation constraints.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to validate.
   * @param array $fields_to_validate
   *   The fields to validate.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException
   *   If validation errors are found.
   */
  public function validate(EntityInterface $entity, array $fields_to_validate = []) {
    // @todo Remove when https://www.drupal.org/node/2164373 is committed.
    if (!$entity instanceof FieldableEntityInterface) {
      return;
    }
    $violations = $entity->validate();

    // Remove violations of inaccessible fields as they cannot stem from our
    // changes.
    $violations->filterByFieldAccess();

    if ($violations->count() > 0) {
      $message = "Unprocessable Entity: validation failed.\n";
      foreach ($violations as $violation) {
        // We strip every HTML from the error message to have a nicer to read
        // message on REST responses.
        $fieldMachineName = $violation->getPropertyPath();
        if (strpos($fieldMachineName, '.') !== FALSE) {
          $split = explode('.', $fieldMachineName);
          $fieldMachineName = $split[0];
        }
        $value = '';
        // Get field value.
        if ($entity->hasField($fieldMachineName)) {
          $fieldValue = $entity->get($fieldMachineName)->getValue();
          if (!empty($fieldValue[0]) && !empty($fieldValue[0]['value'])) {
            $value = $fieldValue[0]['value'];
          }
        }
        // If value is unsupported, means that the server
        // does not know how to interpret this field and it is
        // no need to validate it.
        if ($value !== "unsupported") {
          $message .= $violation->getPropertyPath() . ': ' . PlainTextOutput::renderFromHtml($violation->getMessage()) . "\n";
        }
      }
      if ($message !== "Unprocessable Entity: validation failed.\n") {
        throw new UnprocessableEntityHttpException($message);
      }
    }
  }

}
