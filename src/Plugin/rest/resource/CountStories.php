<?php

namespace Drupal\openstory\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides a resource to count stories.
 *
 * @RestResource(
 *   id = "get_count_stories",
 *   label = @Translation("Count stories"),
 *   uri_paths = {
 *     "canonical" = "/get_count_stories/{days}"
 *   }
 * )
 */
class CountStories extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('custom_rest'),
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Count all nodes of a content type.
   *
   * @param string $days
   *   Filter on x days.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   */
  public function get($days) {
    // Get stories from current date - $days days.
    $today = strtotime("today");
    $date = strtotime("-" . $days . " days");
    $query = $this->entityTypeManager->getStorage('node')->getAggregateQuery();
    $query->condition('created', $date, '>');
    $query->aggregate("type", 'COUNT');
    $query->groupBy("type");
    $result = $query->execute();
    $storiesToday = $this->createBundleArray($result);

    $bundleToday = [];
    foreach ($storiesToday as $todayBundleName => $bundleStatsToday) {
      $bundleToday[$todayBundleName] = $bundleStatsToday;
    }

    // Get stories from current date - $days * 2 days in order to see
    // if the number of nodes of a content type increased or decreased.
    $date = strtotime("-" . $days * 2 . " days");
    $query1 = $this->entityTypeManager->getStorage('node')->getAggregateQuery();
    $query1->condition('created', $date, '>=');
    $query1->condition('created', $today, '<');
    $query1->aggregate("type", 'COUNT');
    $query1->groupBy("type");
    $result1 = $query1->execute();
    $storiesYesterday = $this->createBundleArray($result1);

    $bundleYesterday = [];
    foreach ($storiesYesterday as $yesterdayBundleName => $bundleStatsYesterday) {
      $bundleYesterday[$yesterdayBundleName] = $bundleStatsYesterday;
    }

    // Get count of all stories created today.
    $allNewStories = 0;
    foreach ($storiesToday as $storyToday) {
      $allNewStories += $storyToday['type_count'];
    }

    // Get totals.
    $totals = $this->getNodeTotals();

    $stories = [];
    $stories['All Stories']['todaysNewStories'] = (int) $allNewStories;
    $stories['All Stories']['totalStories'] = (int) $totals;

    foreach ($storiesToday as $bundle => $count) {
      $bundleName = $this->entityTypeManager->getStorage('node_type')->load($bundle)->get('name');
      $todayReport = isset($storiesToday[$bundle]) ? (int) $storiesToday[$bundle] : 0;
      $yesterdayReport = isset($storiesYesterday[$bundle]) ? (int) $storiesYesterday[$bundle] : 0;
      // Calculate Delta.
      $delta = $todayReport - $yesterdayReport;
      // Add info to response.
      $stories[$bundleName] = ['delta' => $delta];
    }

    $totalBundles = $this->getBundleTotals();

    foreach ($totalBundles as $bundle => $totalCount) {
      if (!isset($stories[$bundle]['delta'])) {
        // Add 0 on delta if there is no change
        // For consistency in the reply.
        $stories[$bundle]['delta'] = 0;
      }
      $stories[$bundle]['totalBundle'] = (int) $totalCount;
    }

    // $stories['Other'] = ['other' => $allStories - $other];
    $response = new ResourceResponse($stories, 200);
    return $response;

  }

  /**
   * Create array from query entity result.
   *
   * @param array $queryArray
   *   Array result from entity query.
   *
   * @return array
   *   Return array.
   */
  private function createBundleArray(array $queryArray) {
    $result = [];
    if (!empty($queryArray)) {
      foreach ($queryArray as $item => $content) {
        if (!empty($content)) {
          $result[$content['type']] = $content['type_count'];
        }
      }
    }
    return $result;
  }

  /**
   * Gets the complete number of nodes.
   */
  private function getNodeTotals() {
    $totals = 0;
    // Get all node counts.
    $query = $this->entityTypeManager->getStorage('node')->getAggregateQuery();
    $totals = $query->count()->execute();

    return $totals;
  }

  /**
   * Gets the complete number of nodes grouped by content type label.
   */
  private function getBundleTotals() {
    $bundleTotals = [];

    $bundles = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    // Get total nodes grouped by bundle.
    foreach ($bundles as $bundle => $bundleInfo) {
      $nodes = $this->entityTypeManager->getStorage('node')->loadByProperties(['type' => $bundle]);
      $bundleTotals[$bundleInfo->label()] = count($nodes);
    }
    return $bundleTotals;
  }

}
