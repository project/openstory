<?php

namespace Drupal\openstory\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\Entity\User;

/**
 * Provides a resource to reset the user password.
 *
 * @RestResource(
 *   id = "reset_user_password",
 *   label = @Translation("Reset user password"),
 *   uri_paths = {
 *     "canonical" = "/reset_user_password",
 *     "https://www.drupal.org/link-relations/create" = "/reset_user_password"
 *   }
 * )
 */
class ResetUserPassword extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      array $serializer_formats,
      LoggerInterface $logger,
      AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $configuration,
        $plugin_id,
        $plugin_definition,
        $container->getParameter('serializer.formats'),
        $container->get('logger.factory')->get('custom_rest'),
        $container->get('current_user')
    );
  }

  /**
   * Responds to entity POST requests and returns a link to reset user password.
   *
   * @param string $data
   *   Data.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function post($data) {
    if (!empty($data['name'][0]) || !empty($data['mail'][0])) {
      if (!empty($data['name'][0])) {
        $name = $data['name'][0]['value'];
        $uidArray = $this->getUserId('name', $name);
        // Return value from array which always will have just one element.
        $uid = array_pop($uidArray);
        if ($uid) {
          $account = User::load($uid);
          $langCode = $account->getPreferredLangcode();
          _user_mail_notify('password_reset', $account, $langCode);
          $reset['reset_password'] = TRUE;
          return new ResourceResponse($reset, 200);
        }
      }
      // Case when name is not inserted or is wrong.
      if (!empty($data['mail'][0])) {
        $mail = $data['mail'][0]['value'];
        $uidArray = $this->getUserId('mail', $mail);
        // Return value from array which always will have just one element.
        $uid = array_pop($uidArray);
        if ($uid) {
          $account = User::load($uid);
          $langCode = $account->getPreferredLangcode();
          _user_mail_notify('password_reset', $account, $langCode);
          $reset['reset_password'] = TRUE;
          return new ResourceResponse($reset, 200);
        }
      }
    }
    else {
      throw new BadRequestHttpException('Data must contain user name or user mail.');
    }
  }

  /**
   * Return the user id based on name or email.
   *
   * @param string $field
   *   Name or email.
   * @param string $fieldValue
   *   Name or email value.
   *
   * @return array
   *   The array containing the user id.
   */
  private function getUserId($field, $fieldValue) {
    $query = \Drupal::entityQuery('user');
    $query->condition($field, $fieldValue);
    $uid = $query->execute();
    return $uid;
  }

}
