<?php

namespace Drupal\openstory\Plugin\rest\resource;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\rest\Plugin\rest\resource\EntityResource;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\field_collection\Entity\FieldCollectionItem;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Represents entities as resources.
 *
 * @see \Drupal\openstory\Plugin\Deriver\MyEntityDeriver
 *
 * @RestResource(
 *   id = "create_entities",
 *   label = @Translation("Create entities."),
 *   serialization_class = "Drupal\Core\Entity\Entity",
 *   deriver = "Drupal\openstory\Plugin\Deriver\MyEntityDeriver",
 *   uri_paths = {
 *     "canonical" = "/create_entities/{entity_type}/{entity}",
 *     "https://www.drupal.org/link-relations/create" = "/create_entities/{entity_type}"
 *   }
 * )
 */
class CreateEntities extends EntityResource {

  use CreateEntityResourceValidationTrait;
  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, array $serializer_formats, LoggerInterface $logger, ConfigFactoryInterface $config_factory, PluginManagerInterface $link_relation_type_manager, EntityFieldManagerInterface $entityFieldManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $serializer_formats, $logger, $config_factory, $link_relation_type_manager);
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('config.factory'),
      $container->get('plugin.manager.link_relation_type'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The entity.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post(EntityInterface $entity = NULL) {
    $fields = $entity->getFields();
    $fieldCollectionItemArray = [];
    $fieldParagraphArray = [];
    foreach ($fields as $field_key => $field_value) {
      // TODO: change this evaluation.
      if (strpos($field_key, 'field_') !== FALSE) {
        $fieldDefinition = $fields[$field_key]->getDataDefinition();
        $fieldType = $fieldDefinition->getType();
        if ($fieldType == 'field_collection') {
          $fieldCollectionItemArray[] = $this->createFieldCollection($fields, $field_key, $entity);
        }
        elseif ($fieldType == "entity_reference_revisions") {
          $fieldParagraphData = $fields[$field_key]->getValue();
          foreach ($fieldParagraphData as $fieldParagraphDataKey => $fieldParagraphDataValue) {
            $fieldParagraphArray[$field_key] = $fieldParagraphDataValue;
          }
        }
      }
    }
    if (!empty($fieldParagraphArray)) {
      foreach ($fieldParagraphArray as $field_key => $value) {
        if (!empty($fieldParagraphArray[$field_key]['type'])) {
          $this->createParagraph($value, $fieldParagraphArray[$field_key]['type'], $field_key, $entity);
        }
      }
    }

    parent::post($entity);
    $this->saveFieldCollection($fieldCollectionItemArray, $entity);
    return new ModifiedResourceResponse($entity, 200);
  }

  /**
   * Responds to entity PATCH requests.
   *
   * @param \Drupal\Core\Entity\EntityInterface $original_entity
   *   The original entity object.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function patch(EntityInterface $original_entity, EntityInterface $entity = NULL) {

    $original_entity_clone = $original_entity;
    $fields = $entity->getFields();
    $ignoreFields = [];
    $fieldCollectionItemArray = [];
    foreach ($fields as $field_key => $field_value) {
      // TODO: change this evaluation.
      if (strpos($field_key, 'field_') !== FALSE) {
        $fieldDefinition = $fields[$field_key]->getDataDefinition();
        $fieldType = $fieldDefinition->getType();
        if ($fieldType == 'field_collection') {
          $collectionItem = $this->updateFieldCollection($fields[$field_key]->getValue(), $field_key, $original_entity, $entity, $ignoreFields);
          if (!empty($collectionItem)) {
            $fieldCollectionItemArray[] = $collectionItem;
          }
        }
        elseif ($fieldType == "entity_reference_revisions") {
          $this->updateParagraphFields($fields[$field_key]->getValue(), $field_key, $original_entity, $ignoreFields);
        }
      }
    }

    // Set values on other fields than field collection and paragrah.
    // Because original contains the old values.
    $entitySubmittedValues = $entity->_restSubmittedFields;
    foreach ($entitySubmittedValues as $key => $value) {
      if (!array_key_exists($value, $ignoreFields)) {
        $original_entity->set($value, $entity->get($value)->getValue());
      }
    }
    // Set _restSubmittedFields on original_entity.
    // Needed in parent patch.
    $original_entity->__set("_restSubmittedFields", $entitySubmittedValues);

    parent::patch($original_entity_clone, $original_entity);
    $this->saveFieldCollection($fieldCollectionItemArray, $original_entity);
    return new ModifiedResourceResponse($original_entity, 200);
  }

  /**
   * Create field collection item.
   *
   * @param array $fieldCollectionItemArray
   *   Entity fields.
   * @param \Drupal\Core\Entity\EntityInterface $original_entity
   *   The entity.
   */
  private function saveFieldCollection(array $fieldCollectionItemArray, EntityInterface &$original_entity) {
    if (!empty($fieldCollectionItemArray)) {
      foreach ($fieldCollectionItemArray as $key => $value) {
        $value['item']->setHostEntity($original_entity);
        $value['item']->save();
        if (!empty($value['collection'])) {
          foreach ($value['collection'] as $fieldCollection => $fieldValue) {
            if (array_key_exists($fieldCollection, $value['data'][0])) {
              $this->createFieldCollectionNested($fieldCollection, $value['data'][0][$fieldCollection], $value['item']);
            }
          }
        }
        if (!empty($value['paragraph'])) {
          foreach ($value['paragraph'] as $fieldParagraph => $fieldValue) {
            if (array_key_exists($fieldParagraph, $value['data'][0])) {
              $this->createParagraph($value['data'][0][$fieldParagraph][0], $value['data'][0][$fieldParagraph][0]['type'], $fieldParagraph, $value['item']);
            }
          }
        }
      }
    }
  }

  /**
   * Create field collection item.
   *
   * @param array $entityFields
   *   Entity fields.
   * @param array $nestedCollection
   *   Nested field collections.
   * @param array $nestedParagraph
   *   Nested paragraphs.
   * @param array $dataFields
   *   The entity data fields.
   * @param array $collections
   *   Array that store field collections.
   * @param array $paragraphs
   *   Array that store paragraphs.
   * @param array $entityArray
   *   Array storing fields from a complex field.
   */
  private function getFieldsToBeSaved(array $entityFields, array &$nestedCollection, array &$nestedParagraph, array $dataFields, array &$collections, array &$paragraphs, array &$entityArray) {
    $this->checkNestedFieldCollectionOrParagraph($entityFields, $nestedCollection, $nestedParagraph);
    $nestedCollection = array_combine($nestedCollection, $nestedCollection);
    $nestedParagraph = array_combine($nestedParagraph, $nestedParagraph);

    foreach ($dataFields[0] as $key_v => $value_v) {
      if (array_key_exists($key_v, $entityFields)) {
        if (!array_key_exists($key_v, $nestedCollection) && !array_key_exists($key_v, $nestedParagraph)) {
          $entityArray[$key_v] = $value_v;
        }
        elseif (array_key_exists($key_v, $nestedCollection)) {
          $collections[] = $key_v;
        }
        elseif (array_key_exists($key_v, $nestedParagraph)) {
          $paragraphs[] = $key_v;
        }
      }
    }
  }

  /**
   * Create field collection item.
   *
   * @param array $entityFields
   *   Entity fields.
   * @param array $nestedCollection
   *   Nested field collections.
   * @param array $nestedParagraph
   *   Nested paragraphs.
   * @param array $dataFields
   *   The entity data fields.
   * @param array $collections
   *   Array that store field collections.
   * @param array $paragraphs
   *   Array that store paragraphs.
   * @param \Drupal\Core\Entity\EntityInterface $entityItem
   *   The entity item.
   */
  private function getFieldsToBeUpdated(array $entityFields, array &$nestedCollection, array &$nestedParagraph, array $dataFields, array &$collections, array &$paragraphs, EntityInterface &$entityItem) {
    $nestedCollection = array_combine($nestedCollection, $nestedCollection);
    $nestedParagraph = array_combine($nestedParagraph, $nestedParagraph);
    foreach ($dataFields[0] as $key_v => $value_v) {
      if (array_key_exists($key_v, $entityFields)) {
        if (!array_key_exists($key_v, $nestedParagraph) && !array_key_exists($key_v, $nestedCollection)) {
          $entityItem->set($key_v, $value_v);
        }
        elseif (array_key_exists($key_v, $nestedCollection)) {
          $collections[] = $key_v;
        }
        elseif (array_key_exists($key_v, $nestedParagraph)) {
          $paragraphs[] = $key_v;
        }
      }
    }
  }

  /**
   * Create field collection item.
   *
   * @param array $fields
   *   Entity fields.
   * @param string $fieldName
   *   The field name needed to connect the entity with field collection.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return array|null
   *   Form collection item.
   */
  private function createFieldCollection(array $fields, $fieldName, EntityInterface $entity) {
    // Data extracted from json.
    $fieldCollectionData = $fields[$fieldName]->getValue();
    // Array that store simple fields to be added to field collection item.
    $collectionArray = [];
    $collectionArray['field_name'] = $fieldName;
    $collectionArray['host_type'] = $entity->getEntityTypeId();
    // Arrays that store nested fields collection or paragraphs.
    $nestedCollection = [];
    $nestedParagraph = [];
    $collections = [];
    $paragraphs = [];
    // Get all collection fields to see if
    // contains nested fields collection or paragraphs.
    $collectionFields = $this->entityFieldManager->getFieldDefinitions('field_collection_item', $fieldName);
    $this->getFieldsToBeSaved($collectionFields, $nestedCollection, $nestedParagraph, $fieldCollectionData, $collections, $paragraphs, $collectionArray);
    $fieldCollectionItem = FieldCollectionItem::create($collectionArray);
    $result = [
      "item" => $fieldCollectionItem,
      "collection" => array_combine($collections, $collections),
      "paragraph" => array_combine($paragraphs, $paragraphs),
      "data" => $fieldCollectionData,
    ];
    return $result;
  }

  /**
   * Create field collection item.
   *
   * @param string $fieldName
   *   The field name.
   * @param string $fieldCollectionData
   *   Fields wanted to be added to entity.
   * @param \Drupal\Core\Entity\EntityInterface $entityHost
   *   Entity form.
   */
  private function createFieldCollectionNested($fieldName, $fieldCollectionData, EntityInterface $entityHost) {
    // Array that store simple fields to be added to field collection item.
    $collectionArray = [];
    // Arrays that store nested fields collection or paragraphs.
    $nestedCollection = [];
    $nestedParagraph = [];
    $collections = [];
    $paragraphs = [];
    $collectionArray['field_name'] = $fieldName;
    $collectionArray['host_type'] = $entityHost->getEntityTypeId();
    // Get all collection fields to see if contains
    // nested fields collection or paragraphs.
    $collectionFields = $this->entityFieldManager->getFieldDefinitions('field_collection_item', $fieldName);
    $this->getFieldsToBeSaved($collectionFields, $nestedCollection, $nestedParagraph, $fieldCollectionData, $collections, $paragraphs, $collectionArray);

    $fieldCollectionItem = FieldCollectionItem::create($collectionArray);
    $fieldCollectionItem->setHostEntity($entityHost);
    $fieldCollectionItem->save();
    // If there are nested fields collections, recall function.
    if (!empty($collections)) {
      foreach ($collections as $key => $value) {
        $this->createFieldCollectionNested($value, $fieldCollectionData[0][$value], $fieldCollectionItem);
      }
    }
    // If there are nested paragraphs, recall function.
    if (!empty($paragraphs)) {
      foreach ($paragraphs as $paragraphKey => $paragraphValue) {
        $this->createParagraph($fieldCollectionData[0][$paragraphValue][0], $fieldCollectionData[0][$paragraphValue][0]['type'], $paragraphValue, $fieldCollectionItem);
      }
    }
    // Set nested field to collection and save it.
    $itemId = $fieldCollectionItem->get('item_id')->getValue();
    $itemValue = [
      'field_collection_item' => $fieldCollectionItem,
      'value' => $itemId[0]['value'],
      'revision_id' => $fieldCollectionItem->getRevisionId(),
    ];
    $entityHost->set($fieldName, $itemValue);
    $entityHost->save();
  }

  /**
   * Check if in entity fields exists field collections or paragraphs.
   *
   * @param array $collectionFields
   *   The collection fields.
   * @param array $nestedCollection
   *   Store nested collection fields.
   * @param array $nestedParagraph
   *   Store nested paragraphs.
   */
  private function checkNestedFieldCollectionOrParagraph(array $collectionFields, array &$nestedCollection, array &$nestedParagraph) {
    foreach ($collectionFields as $key => $value) {
      if (strpos($key, 'field_') !== FALSE) {
        if ($value->getType() == 'field_collection') {
          $nestedCollection[] = $key;
        }
        elseif ($value->getType() == "entity_reference_revisions") {
          $nestedParagraph[] = $key;
        }
      }
    }
  }

  /**
   * Check if in entity fields exists field collections or paragraphs.
   *
   * @param array $value
   *   The entity fields.
   * @param string $paragraphType
   *   The paragraph type.
   * @param string $fieldName
   *   The field name.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  private function createParagraph(array $value, $paragraphType, $fieldName, EntityInterface &$entity) {
    $paragraphFields = $this->entityFieldManager->getFieldDefinitions('paragraph', $paragraphType);
    // Arrays that store nested fields collection or paragraphs.
    $nestedCollection = [];
    $nestedParagraph = [];
    $collections = [];
    $paragraphs = [];
    // Check if within entity fields are nested fields collection or paragraphs.
    $this->checkNestedFieldCollectionOrParagraph($paragraphFields, $nestedCollection, $nestedParagraph);
    $nestedCollection = array_combine($nestedCollection, $nestedCollection);
    $nestedParagraph = array_combine($nestedParagraph, $nestedParagraph);
    $values = [];
    if (is_array($value)) {
      foreach ($value as $key_p => $value_p) {
        if (array_key_exists($key_p, $paragraphFields) || $key_p == 'type') {
          if (!array_key_exists($key_p, $nestedParagraph) && !array_key_exists($key_p, $nestedCollection)) {
            $values[$key_p] = $value_p;
          }
          elseif (array_key_exists($key_p, $nestedCollection)) {
            $collections[] = $key_p;
          }
          elseif (array_key_exists($key_p, $nestedParagraph)) {
            $paragraphs[] = $key_p;
          }
        }
      }
      $paragraph = Paragraph::create($values);
      $paragraph->save();
      if (!empty($paragraphs)) {
        foreach ($paragraphs as $paragraphKey => $paragraphValue) {
          $this->createParagraph($value[$paragraphValue][0], $value[$paragraphValue][0]['type'], $paragraphValue, $paragraph);
        }
      }

      $entity->$fieldName = [
        [
          'target_id' => $paragraph->id(),
          'target_revision_id' => $paragraph->getRevisionId(),
        ],
      ];
      if ($entity instanceof FieldCollectionItem || $entity instanceof Paragraph) {
        $entity->save();
      }
      if (!empty($collections)) {
        foreach ($collections as $collectionKey => $collectionValue) {
          $this->createFieldCollectionNested($collectionValue, $value[$collectionValue], $paragraph);
        }
      }
    }
  }

  /**
   * Update paragraphs.
   *
   * @param array $paragraphData
   *   The paragraph data.
   * @param string $fieldName
   *   The field name.
   * @param \Drupal\Core\Entity\EntityInterface $originalEntity
   *   The entity.
   * @param array $ignoreFields
   *   The array to ignore complex fields.
   */
  private function updateParagraphFields(array $paragraphData, $fieldName, EntityInterface &$originalEntity, array &$ignoreFields) {
    $ignoreFields[$fieldName] = $fieldName;
    $paragraphItemList = $originalEntity->$fieldName;
    $fieldParagraph = $paragraphItemList[0];
    $fieldParagraphItem = Paragraph::load($fieldParagraph->target_id);
    if (!empty($fieldParagraphItem)) {
      $paragraphType = $fieldParagraphItem->get('type')->getValue();
      $paragraphFields = $this->entityFieldManager->getFieldDefinitions('paragraph', $paragraphType[0]['target_id']);
      // Arrays that store nested fields collection or paragraphs.
      $nestedCollection = [];
      $nestedParagraph = [];
      $collections = [];
      $paragraphs = [];
      // Check if within entity fields are
      // nested fields collection or paragraphs.
      $this->checkNestedFieldCollectionOrParagraph($paragraphFields, $nestedCollection, $nestedParagraph);
      $this->getFieldsToBeUpdated($paragraphFields, $nestedCollection, $nestedParagraph, $paragraphData, $collections, $paragraphs, $fieldParagraphItem);
      $fieldParagraphItem->save();
      // If there are nested paragraphs, recall function.
      if (!empty($paragraphs)) {
        foreach ($paragraphs as $paragraphKey => $paragraphValue) {
          if (array_key_exists($paragraphValue, $paragraphData[0])) {
            $this->updateParagraphFields($paragraphData[0][$paragraphValue], $paragraphValue, $fieldParagraphItem, $ignoreFields);
          }
        }
      }
      // If there are nested fields collections, recall function.
      if (!empty($collections)) {
        foreach ($collections as $collectionKey => $collectionValue) {
          $this->updateFieldCollection($paragraphData[0][$collectionValue], $collectionValue, $fieldParagraphItem, $fieldParagraphItem, $ignoreFields);
        }
      }
    }
  }

  /**
   * Update field collection.
   *
   * @param array $fieldCollectionData
   *   The paragraph data.
   * @param string $fieldName
   *   The field name.
   * @param \Drupal\Core\Entity\EntityInterface $original_entity
   *   The original entity.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param array $ignoreFields
   *   The array to ignore complex fields.
   *
   * @return array|null
   *   Form collection item.
   */
  private function updateFieldCollection(array $fieldCollectionData, $fieldName, EntityInterface $original_entity, EntityInterface &$entity, array &$ignoreFields) {
    if (!empty($fieldCollectionData[0])) {
      $ignoreFields[$fieldName] = $fieldName;
      $fieldItemList = $original_entity->$fieldName;
      $fieldCollection = $fieldItemList[0];
      $fieldCollectionItem = FieldCollectionItem::load($fieldCollection->value);
      if (!empty($fieldCollectionItem)) {
        // Update field collection.
        $collectionFields = $this->entityFieldManager->getFieldDefinitions('field_collection_item', $fieldName);
        // Arrays that store nested fields collection or paragraphs.
        $nestedCollection = [];
        $nestedParagraph = [];
        $collections = [];
        $paragraphs = [];
        $collectionArray['field_name'] = $fieldName;
        $collectionArray['host_type'] = $original_entity->getEntityTypeId();
        // Get all collection fields to see if contains
        // nested fields collection or paragraphs.
        $this->checkNestedFieldCollectionOrParagraph($collectionFields, $nestedCollection, $nestedParagraph);
        $this->getFieldsToBeUpdated($collectionFields, $nestedCollection, $nestedParagraph, $fieldCollectionData, $collections, $paragraphs, $fieldCollectionItem);
        // If there are nested fields collections, recall function.
        if (!empty($collections)) {
          foreach ($collections as $key => $value) {
            $this->updateFieldCollection($fieldCollectionData[0][$value], $value, $fieldCollectionItem, $fieldCollectionItem, $ignoreFields);
          }
        }
        // If there are nested paragraphs, recall function.
        if (!empty($paragraphs)) {
          foreach ($paragraphs as $paragraphKey => $paragraphValue) {
            $this->updateParagraphFields($fieldCollectionData[0][$paragraphValue], $paragraphValue, $fieldCollectionItem, $ignoreFields);
          }
        }
      }
      else {
        // Create field collection item.
        return $this->createFieldCollection($entity->getFields(), $fieldName, $entity);
      }
    }
  }

}
